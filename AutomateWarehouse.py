import logging
import boto3
from boto3.s3.transfer import TransferConfig
import csv
import pymssql
import datetime
import io

# Getting the database connection to MSSQL using PYMSSQL


def get_db_connection():
    return pymssql.connect(server="reports6.autotask.net", user="corpcloud",
                           password="SWWaFWFSUFRv", database="TF_702378_WH", port=1433)


# Getting the views from the database

def get_views():
    conn = get_db_connection()
    cursor = conn.cursor()
    try:
        select_table = "SELECT * FROM information_schema.views"
        cursor.execute(select_table)
        return cursor.fetchall()
    finally:
        cursor.close()
        conn.close()


# Getting the data from the MSSQL database

def get_data(view_name):
    conn = get_db_connection()
    cursor = conn.cursor()
    try:
        select_table = f"SELECT * FROM \"{view_name}\""
        cursor.execute(select_table)
        in_memory = io.StringIO()
        rows = [[col[0] for col in cursor.description]]
        for row in cursor:
            rows.extend(cursor.fetchall())  # more efficient memory
        csv.writer(in_memory).writerows(rows)
        return in_memory.getvalue().encode('utf-8')
    finally:
        cursor.close()
        conn.close()


# Writing to the S3 bucket

def write_to_s3(table_name: str, data: bytes):
    date_str = datetime.datetime.now().strftime("%Y%m%d")
    s3 = boto3.client('s3')
    s3.put_object(
        Body=data,
        Bucket='automatewarehouse',
        Key=f"/AutoTask/{date_str}/{table_name}/{table_name}-{date_str}-data.csv"
    )
    config = boto3.s3.transfer.TransferConfig(
        multipart_threshold=128 * 2048 * 2048,
        max_concurrency=10,
        num_download_attempts=10,
        multipart_chunksize=16 * 2048 * 2048,
        max_io_queue=10000
    )


# The main program

if __name__ == "__main__":
    views = get_views()
    for view in views:
        table_name = view[2][3:]
        data = get_data(view[2])
        print(table_name)
        print(len(data))
        write_to_s3(table_name, data)
